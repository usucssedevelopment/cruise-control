# cruise-control

This repository is intended to show artifacts from analysis, design, and eventually implementation activities of
project that aims to build a cruise control system. In a real agile project, all these artifacts would
evolve from a series Sprints or rapid iterations.   Perhaps, the earlier Sprints would focus on analysis more, but
they would not proclude doing work on design or even some of the implementation (typically as an early prototype).
Similarly, the middle Sprints would focus on detail, but may still involve analysis and implementation.  And, the
later Sprints would focus on implementation, but allow for changes to analysis and design concepts, as needed.

Here, we separate the work artifacts into three sub-directories that loosely represent this three general phases
of emphasis.   There contents is as follows:

   |Subdirectory|Content description|
   |------------|-------------------|
   |analysis|Concept document, conceptual model from an analysis prospective, a prototype that helps explore one of the key features|
   |design|Conceptual model from a design prospective, a prototype that helps explore a design decision|
   |implementation|Software intended to evolve into the final product|
   
   
	

